import sys

from twitterhelper import TwitterHelper

if __name__ == "__main__":
    APP_KEY = ""
    APP_SECRET = ""
    OAUTH_TOKEN = ""
    OAUTH_TOKEN_SECRET = ""

    tw = TwitterHelper(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)

    tweet_id = sys.argv[1]

    markdown_options = {
        "header": "# Twitter thread for tweet ID %s</h1>\n" % tweet_id,
        "mention_link_attribs": '{:target="_blank"}',
        "hashtag_link_attribs": '{:target="_blank"}',
        "symbol_link_attribs": '{:target="_blank"}',
        "url_link_attribs": '{:target="_blank"}',
        "media_attribs": '{:class="text-center lazyload"}',
        "footer": "\n\nEnd of the thread</p>",
    }

    html_options = {
        "header": "<h1>Twitter thread for tweet ID %s</h1>\n" % tweet_id,
        "mention_link_attribs": 'target="_blank"',
        "hashtag_link_attribs": 'target="_blank"',
        "symbol_link_attribs": 'target="_blank"',
        "url_link_attribs": 'target="_blank"',
        "media_attribs": 'class="text-center lazyload"',
        "footer": "\n<p>End of the thread</p>",
    }

    # Thread
    tw.save_thread(tweet_id, markdown_options, html_options)
