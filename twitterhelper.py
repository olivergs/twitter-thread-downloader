# auth = tweepy.OAuthHandler("consumer_key", "consumer_secret")

# # Redirect user to Twitter to authorize
# redirect_user(auth.get_authorization_url())

# # Get access token
# auth.get_access_token("verifier_value")

# # Construct the API instance
# api = tweepy.API(auth)

import json
import os

import tweepy
import wget


class TwitterHelper:

    BASE_USER_URL = "https://twitter.com/%s"
    BASE_HASHTAG_URL = "https://twitter.com/search?q=%%23%s"
    BASE_SYMBOL_URL = "https://twitter.com/search?q=%%24%s"

    DEFAULT_MARKDOWN_OPTIONS = {
        "header": "",
        "mention_link_attribs": "",
        "hashtag_link_attribs": "",
        "symbol_link_attribs": "",
        "url_link_attribs": "",
        "media_attribs": "",
        "footer": "",
    }

    DEFAULT_HTML_OPTIONS = {
        "header": "",
        "mention_link_attribs": "",
        "hashtag_link_attribs": "",
        "symbol_link_attribs": "",
        "url_link_attribs": "",
        "media_attribs": "",
        "footer": "",
    }

    def __init__(self, app_key, app_secret, oauth_token, oauth_token_secret):
        auth = tweepy.OAuthHandler(app_key, app_secret)
        auth.set_access_token(oauth_token, oauth_token_secret)
        self.api = tweepy.API(auth)

    def get_tweet(self, id):
        return self.api.get_status(id=id, tweet_mode="extended")

    def get_thread(self, id):
        # update these for whatever tweet you want to process replies to
        main_tweet = self.api.get_status(id=id, tweet_mode="extended")
        name = main_tweet.user.screen_name

        thread = [main_tweet]

        def recursive_find_replies(tweet_id, thread):
            print("Buscando respuestas para ", tweet_id)
            subthread = self.get_replies_to(
                tweet_id, custom_query="to:@" + name + " from:@" + name
            )["replies"]
            # print("Replies to:", tweet_id)
            if subthread:
                for reply in subthread:
                    if reply not in thread:
                        thread.append(reply)
                        # print("\t", reply.id_str, " - ", reply.full_text)
                        subthread.extend(recursive_find_replies(reply.id_str, thread))
            return subthread

        recursive_find_replies(id, thread)
        return thread

    def get_replies_to(self, id, custom_query=None):
        main_tweet = self.api.get_status(id=id, tweet_mode="extended")
        if custom_query is not None:
            query = custom_query
        else:
            name = main_tweet.user.screen_name
            query = "to:@" + name
        # update these for whatever tweet you want to process replies to
        replies = []
        for tweet in tweepy.Cursor(
            self.api.search, q=query, since_id=id, timeout=999999, tweet_mode="extended"
        ).items(1000):
            # result_type='recent'
            if hasattr(tweet, "in_reply_to_status_id_str"):
                if tweet.in_reply_to_status_id_str == id:
                    replies.append(tweet)
        return {"tweet": main_tweet, "replies": replies}

    def format_tweet_markdown(self, tweet, markdown_options=DEFAULT_MARKDOWN_OPTIONS):
        substitutions = []
        display_range = tweet.display_text_range
        text = tweet.full_text[display_range[0] : display_range[1]]

        # User mention substitutions
        for mention in tweet.entities["user_mentions"]:
            start, end = mention["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                to_replace = text[start:end]
                user_url = self.BASE_USER_URL % mention["screen_name"]
                replacement = "[{}]({}){}".format(
                    to_replace, user_url, markdown_options["mention_link_attribs"]
                )
                substitutions.append([start, end, replacement])

        # Hashtag substitutions
        for hashtag in tweet.entities["hashtags"]:
            start, end = hashtag["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                to_replace = text[start:end]
                hashtag_url = self.BASE_HASHTAG_URL % hashtag["text"]
                replacement = "[{}]({}){}".format(
                    to_replace, hashtag_url, markdown_options["hashtag_link_attribs"]
                )
                substitutions.append([start, end, replacement])

        # Symbol substitutions
        for symbol in tweet.entities["symbols"]:
            start, end = symbol["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                to_replace = text[start:end]
                symbol_url = self.BASE_SYMBOL_URL % symbol["text"]
                replacement = "[{}]({}){}".format(
                    to_replace, symbol_url, markdown_options["symbol_link_attribs"]
                )
                substitutions.append([start, end, replacement])

        # URL substitutions
        for url_entity in tweet.entities["urls"]:
            start, end = url_entity["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                expanded_url = url_entity["expanded_url"]
                url = url_entity["url"]
                replacement = "[{}]({}){}".format(
                    url, expanded_url, markdown_options["url_link_attribs"]
                )
                substitutions.append([start, end, replacement])

        # Media
        if "media" in tweet.entities:
            for media in tweet.entities["media"]:
                text += "\n\n![{}]({}){}".format(
                    media["display_url"],
                    media["media_url_https"],
                    markdown_options["media_attribs"],
                )

        # Ordering substitutions
        substitutions = sorted(substitutions, key=lambda x: x[0], reverse=True)

        for start, end, replacement in substitutions:
            prev_text = text[:start]
            next_text = text[end:]
            text = prev_text + replacement + next_text
        return markdown_options["header"] + text + markdown_options["footer"]

    def format_tweet_html(self, tweet, html_options=DEFAULT_HTML_OPTIONS):
        substitutions = []
        display_range = tweet.display_text_range
        text = tweet.full_text[display_range[0] : display_range[1]]

        # User mention substitutions
        for mention in tweet.entities["user_mentions"]:
            start, end = mention["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                to_replace = text[start:end]
                user_url = self.BASE_USER_URL % mention["screen_name"]
                replacement = '<a href="{}" {}>{}</a>'.format(
                    user_url, html_options["mention_link_attribs"], to_replace
                )
                substitutions.append([start, end, replacement])

        # Hashtag substitutions
        for hashtag in tweet.entities["hashtags"]:
            start, end = hashtag["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                to_replace = text[start:end]
                hashtag_url = self.BASE_HASHTAG_URL % hashtag["text"]
                replacement = '<a href="{}" {}>{}</a>'.format(
                    hashtag_url, html_options["hashtag_link_attribs"], to_replace
                )
                substitutions.append([start, end, replacement])

        # Symbol substitutions
        for symbol in tweet.entities["symbols"]:
            start, end = symbol["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                to_replace = text[start:end]
                symbol_url = self.BASE_SYMBOL_URL % symbol["text"]
                replacement = '<a href="{}" {}>{}</a>'.format(
                    symbol_url, html_options["symbol_link_attribs"], to_replace
                )
                substitutions.append([start, end, replacement])

        # URL substitutions
        for url_entity in tweet.entities["urls"]:
            start, end = url_entity["indices"]
            if start >= display_range[0]:
                start -= display_range[0]
                end -= display_range[0]
                expanded_url = url_entity["expanded_url"]
                url = url_entity["url"]
                replacement = '<a href="{}" {}>{}</a>'.format(
                    expanded_url, html_options["url_link_attribs"], url
                )
                substitutions.append([start, end, replacement])

        # Media
        if "media" in tweet.entities:
            for media in tweet.entities["media"]:
                text += '\n\n<img src="{}" alt="{}" {}>'.format(
                    media["media_url_https"],
                    media["display_url"],
                    html_options["media_attribs"],
                )

        # Ordering substitutions
        substitutions = sorted(substitutions, key=lambda x: x[0], reverse=True)

        for start, end, replacement in substitutions:
            prev_text = text[:start]
            next_text = text[end:]
            text = prev_text + replacement + next_text
        text = text.replace("\n", "<br>")
        return text

    def download_tweet_media(self, tweet, output=None):
        if "media" in tweet.entities:
            for media in tweet.entities["media"]:
                url = media["media_url_https"]
                wget.download(url, out=output)

    def save_thread(
        self,
        tweet_id,
        markdown_options=DEFAULT_MARKDOWN_OPTIONS,
        html_options=DEFAULT_HTML_OPTIONS,
    ):
        tweets = self.get_thread(id=tweet_id)
        json_data = {}
        markdown_doc = []
        html_doc = []
        base_path = "thread-%s" % tweet_id
        try:
            os.makedirs(base_path)
        except FileExistsError:
            pass
        for tweet in tweets:
            # Add JSON data
            json_data[tweet.id_str] = tweet._json
            # Add markdown data
            markdown_doc.append(self.format_tweet_markdown(tweet, markdown_options))
            # Add HTML data
            html_doc.append(self.format_tweet_html(tweet, html_options))
            # Download media
            self.download_tweet_media(tweet, output=base_path + "/")

        # Write markdown document
        with open(os.path.join(base_path, "thread-%s.md" % tweet_id), "w") as fd:
            markdown_content = "\n\n---\n\n".join(markdown_doc)
            fd.write(
                markdown_options["header"]
                + markdown_content
                + markdown_options["footer"]
            )
            fd.close()

        # Write html document
        with open(os.path.join(base_path, "thread-%s.html" % tweet_id), "w") as fd:
            html_content = (
                '<div class="tweet">'
                + '\n</div>\n\n<div class="tweet">\n'.join(html_doc)
                + "</div>"
            )
            fd.write(html_options["header"] + html_content + html_options["footer"])
            fd.close()

        # Write JSON data
        with open(os.path.join(base_path, "thread-%s.json" % tweet_id), "w") as fd:
            fd.write(json.dumps(json_data, indent=4, sort_keys=True))
            fd.close()
